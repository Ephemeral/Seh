#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdint.h>
#include <stdio.h>
__declspec(naked) void Stub()
{
	__asm
	{
		push ebp
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		pop ebp
		ret
	}
}
LONG WINAPI Filter(
	_In_ struct _EXCEPTION_POINTERS *ExceptionInfo
)
{
	if (ExceptionInfo->ExceptionRecord->ExceptionCode == EXCEPTION_SINGLE_STEP)
	{
		puts("Hit hardware breakpoint!\n");
		ExceptionInfo->ContextRecord->Esp -= 4;
		*(ULONG*)ExceptionInfo->ContextRecord->Esp = ExceptionInfo->ContextRecord->Ebp;
		ExceptionInfo->ContextRecord->Eip += 1;
		puts("Handled the instruction. Continuing\n");
		return EXCEPTION_CONTINUE_EXECUTION;
		
	}
	return EXCEPTION_CONTINUE_SEARCH;
}

int main()
{
	SetUnhandledExceptionFilter(Filter);
	CONTEXT Registers = { 0 };
	Registers.ContextFlags = CONTEXT_DEBUG_REGISTERS;
	GetThreadContext(GetCurrentThread(), &Registers);
	Registers.Dr0 = Stub;
	Registers.Dr7 = 1;
	SetThreadContext(GetCurrentThread(), &Registers);
	Stub();
	fgetc(stdin);
	return 0;
}